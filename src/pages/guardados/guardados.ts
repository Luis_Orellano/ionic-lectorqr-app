import { Component } from "@angular/core";
import { ScanData } from "../../models/scan-data.model";

import { HistorialService } from "../../providers/historial/historial";

@Component({
  selector: "page-guardados",
  templateUrl: "guardados.html",
})
export class GuardadosPage {
  historial: ScanData[] = [];

  constructor(private _historialService: HistorialService) {}

  ionViewDidLoad() {
    this.historial = this._historialService.cargarHistorial();
  }

  abrirScan(index: number) {
    this._historialService.abrirScan(index);
  }
}
