import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { AgmCoreModule } from "@agm/core";

import { MyApp } from "./app.component";
import {
  GuardadosPage,
  MapaPage,
  TabsPage,
  HomePage,
} from "../pages/index.paginas";

import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { HistorialService } from "../providers/historial/historial";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { Contacts } from "@ionic-native/contacts";

@NgModule({
  declarations: [MyApp, HomePage, GuardadosPage, MapaPage, TabsPage],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyA1cszBb5zKRlvUdNNm9EQFvhW8PERbZRI",
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp, HomePage, GuardadosPage, MapaPage, TabsPage],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    HistorialService,
    InAppBrowser,
    Contacts,
  ],
})
export class AppModule {}
